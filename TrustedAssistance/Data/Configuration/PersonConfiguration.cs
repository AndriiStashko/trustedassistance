﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TrustedAssistance.Data.Entities;

namespace TrustedAssistance.Data.Configuration
{
    public class PersonConfiguration : IEntityTypeConfiguration<Person>
    {
        public void Configure(EntityTypeBuilder<Person> builder)
        {
            builder.HasKey(x => x.UserId);
            builder
                .HasOne(a => a.User)
                .WithOne()
                .HasForeignKey<Person>(p => p.UserId)
                .OnDelete(DeleteBehavior.Restrict);

            builder
                .HasOne(a => a.InviterPerson)
                .WithMany().HasForeignKey(x => x.InviterPersonId)
                .OnDelete(DeleteBehavior.Restrict);

            builder
                .HasOne(a => a.TrustedByPerson)
                .WithMany().HasForeignKey(x => x.TrustedByPersonId)
                .OnDelete(DeleteBehavior.Restrict);

            builder.Property(x => x.Contact).HasMaxLength(100).IsRequired();
            builder.Property(x => x.Name).HasMaxLength(100).IsRequired();
            builder.Property(x => x.Skills).HasMaxLength(500).IsRequired();
            builder.Property(x => x.IsBusy).HasDefaultValue(false).IsRequired();
            builder.Ignore(x => x.IsTrusted);
        }
    }
}
