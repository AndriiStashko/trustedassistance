﻿using Microsoft.AspNetCore.Identity;

namespace TrustedAssistance.Data.Entities
{
    public class Person
    {
        public string UserId { get; set; }
        public IdentityUser User { get; set; }
        public string Name { get; set; }
        public string Contact { get; set; }
        public string Skills { get; set; }
        public bool IsBusy { get; set; }
        public string TrustedByPersonId { get; set; }
        public Person TrustedByPerson { get; set; }
        public string InviterPersonId { get; set; }
        public Person InviterPerson { get; set; }

        public bool IsTrusted => !string.IsNullOrEmpty(TrustedByPersonId);
    }
}
