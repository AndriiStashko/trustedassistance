﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TrustedAssistance.Data;
using TrustedAssistance.Data.Entities;

namespace TrustedAssistance.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PersonController : ControllerBase
    {
        private readonly ApplicationDbContext _dbContext;

        public PersonController(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<Person>> Create(CreatePersonRequest request)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var userId = Guid.NewGuid().ToString(); //Get from cookie

            var inviter = _dbContext.Persons.FirstOrDefault(x => x.UserId == request.InviterPersonId);

            if (inviter == null)
            {
                ModelState.AddModelError(nameof(request.InviterPersonId), "Inviter does not exist.");
                return BadRequest(ModelState);
            }

            var person = new Person
            {
                UserId = userId,
                Contact = request.Contact,
                Skills = request.Skills,
                Name = request.Name,
                InviterPersonId = request.InviterPersonId,
            };

            _dbContext.Add(person);
            await _dbContext.SaveChangesAsync();

            return Ok();
        }

        [HttpPost]
        public async Task<ActionResult<PageModel<SearchPersonsResultModel>>> Search(SearchPersonsRequest request)
        {
            if (!IsTrustedPerson())
            {
                return Forbid("Only trusted persons can mark other person as trusted.");
            }

            var query = _dbContext.Persons.Select(x => new SearchPersonsResultModel
            {
                Name = x.Name,
                InviterName = x.InviterPerson.Name,
                Contact = x.Contact,
                Skills = x.Skills,
                IsTrusted = x.IsTrusted,
                IsBusy = x.IsBusy
            });

            if (string.IsNullOrEmpty(request.Name))
            {
                query = query.Where(x => x.Name.Contains(request.Name));
            }

            if (string.IsNullOrEmpty(request.InviterName))
            {
                query = query.Where(x => x.InviterName.Contains(request.InviterName));
            }

            if (string.IsNullOrEmpty(request.Contact))
            {
                query = query.Where(x => x.Contact.Contains(request.Contact));
            }

            if (string.IsNullOrEmpty(request.Skills))
            {
                query = query.Where(x => x.Skills.Contains(request.Skills));
            }

            if (request.IsTrusted.HasValue)
            {
                query = query.Where(x => x.IsTrusted == request.IsTrusted);
            }

            if (request.IsBusy.HasValue)
            {
                query = query.Where(x => x.IsBusy == request.IsBusy);
            }

            return new PageModel<SearchPersonsResultModel>
            {
                Items = await query.Skip(request.StartRow).Take(request.EndRow - request.StartRow).ToListAsync(),
                TotalItems = await query.CountAsync()
            };
        }

        [HttpPost]
        public async Task<ActionResult<PageModel<SearchPersonsResultModel>>> MarkAsTrusted([FromQuery] string personId)
        {
            if (!IsTrustedPerson())
            {
                return Forbid("Only trusted persons can mark other person as trusted.");
            }

            var userId = Guid.NewGuid().ToString(); //Get from cookie
            
            var personToUpdate = _dbContext.Persons.FirstOrDefault(x => x.UserId == personId);
            if (personToUpdate == null)
            {
                ModelState.AddModelError(nameof(personId), "Person does not exist.");
                return BadRequest(ModelState);
            }
            personToUpdate.TrustedByPersonId = userId;

            await _dbContext.SaveChangesAsync();

            return Ok();
        }

        [HttpPost]
        public async Task<ActionResult> SetBusyStatus([FromQuery] string personId, [FromQuery] bool busyStatus)
        {
            if (!IsTrustedPerson())
            {
                return Forbid("Only trusted persons can mark other person as trusted.");
            }

            var personToUpdate = _dbContext.Persons.FirstOrDefault(x => x.UserId == personId);
            if (personToUpdate == null)
            {
                ModelState.AddModelError(nameof(personId), "Person does not exist.");
                return BadRequest(ModelState);
            }
            personToUpdate.IsBusy = busyStatus;

            await _dbContext.SaveChangesAsync();

            return Ok();
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<KeyValuePair<string, string>>>> GetTrusted([FromQuery] string filter)
        {
            var trustedPersons = _dbContext.Persons.Where(x => x.Name.Contains(filter))
            .Select(x => new KeyValuePair<string,string>(x.UserId, x.Name)).ToList();
            return trustedPersons;
        }

        [HttpGet]
        public async Task<ActionResult<PersonResultModel>> GetOwnInfo()
        {
            var userId = Guid.NewGuid().ToString();
            var person = _dbContext.Persons.FirstOrDefault(x => x.UserId == userId);
            if (person == null)
            {
                return Unauthorized();
            }

            return Ok(new PersonResultModel
            {
                Contact = person.Contact,
                InviterPerson = person.InviterPerson.Name,
                IsBusy = person.IsBusy,
                IsTrusted = person.IsTrusted,
                TrustedByUserName = person.Name,
                Name = person.Name,
                Skills = person.Skills,
                UserId = person.UserId
            });
        }

        private bool IsTrustedPerson()
        {
            var userId = Guid.NewGuid().ToString(); //Get from cookie
            var currentUser = _dbContext.Persons.FirstOrDefault(x => x.UserId == userId);

            return currentUser.IsTrusted;
        }
    }

    public class CreatePersonRequest
    {
        [Required]
        [MaxLength(100)]
        public string Name { get; set; }
        [Required]
        [MaxLength(500)]
        public string Skills { get; set; }
        [Required]
        [MaxLength(100)]
        public string Contact { get; set; }
        public string InviterPersonId { get; set; }
    }

    public class SearchPersonsRequest
    {
        public string Name { get; set; }
        public string Skills { get; set; }
        public string Contact { get; set; }
        public string InviterName { get; set; }
        public bool? IsBusy { get; set; }
        public bool? IsTrusted { get; set; }
        public int StartRow { get; set; }
        public int EndRow { get; set; }
    }

    public class SearchPersonsResultModel
    {
        public string Name { get; set; }
        public string Skills { get; set; }
        public string Contact { get; set; }
        public string InviterName { get; set; }
        public bool IsBusy { get; set; }
        public bool IsTrusted { get; set; }
    }

    public class PersonResultModel
    {
        public string UserId { get; set; }
        public string Name { get; set; }
        public string Contact { get; set; }
        public string Skills { get; set; }
        public bool IsBusy { get; set; }
        public string TrustedByUserName { get; set; }
        public string InviterPerson { get; set; }
        public bool IsTrusted { get; set; }
    }


    public class PageModel<T>
    {
        public int TotalItems { get; set; }
        public IEnumerable<T> Items { get; set; }
    }
}
